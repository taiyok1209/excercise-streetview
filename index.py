from flask import Flask, render_template, Response
import time
import key

app = Flask(__name__)

# GPIOをプルアップ抵抗で設定
import RPi.GPIO as GPIO
PIN = 21

@app.route("/stream")
def stream():
    def eventStream():
        pre_input = GPIO.HIGH
        while True:
            input = GPIO.input(PIN)
            if pre_input == GPIO.HIGH and input == GPIO.LOW:
                yield 'data: \n\n'
            pre_input = input
            time.sleep(0.05)
    return Response(eventStream(), mimetype="text/event-stream")


@app.route('/')
def hello_world():
    return render_template('index.html', api_key=key.api)


def setup_gpio():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)


if __name__ == '__main__':
    try:
        setup_gpio()
        app.run(host='0.0.0.0', debug=True)
    finally:
        GPIO.cleanup()
        print('finish')
