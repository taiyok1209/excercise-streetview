var map;
var panorama;

function initMap() {
  startPlace = {
    lat: 35.68,
    lng: 139.76,
    heading: 216.96
  };
  map = new google.maps.Map(document.getElementById('map'), {
    center: startPlace,
    zoom: 15
  });

  panorama = new google.maps.StreetViewPanorama(
    document.getElementById('pano'), {
      position: startPlace,
      pov: {
        heading: startPlace.heading,
        pitch: 0
      }
    });

  map.setStreetView(panorama);

  setServerSendEvent();
}

function difference(link) {
  return Math.abs(panorama.pov.heading % 360 - link.heading);
}

function moveForward() {
  var curr;
  if (!panorama || panorama.links.length == 0) {
    return
  }
  for (i = 0; i < panorama.links.length; i++) {
    if (curr == undefined) {
      curr = panorama.links[i];
    }
    if (difference(curr) > difference(panorama.links[i])) {
      curr = panorama.links[i];
    }
  }
  panorama.setPano(curr.pano);
  if (Math.abs(panorama.pov.heading - curr.heading) < 45) {
    // 45度以内のときは向きを変える
    panorama.setPov({
      heading: curr.heading,
      pitch: 0
    });
  }
  var latlng = panorama.getLocation().latLng;
  map.setCenter(latlng);
}

function setServerSendEvent() {
  // サーバーからメッセージがPUSHされれば前に進む
  var eventSource = new EventSource("/stream");
  eventSource.onmessage = function (e) {
    moveForward();
  };
}